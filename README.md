Type `chrome://extensions` in a tab to bring up the extensions page.

![Extensions - Google Chrome 2015-11-20 20.21.43.png](https://bitbucket.org/repo/6q9eq9/images/1857438340-Extensions%20-%20Google%20Chrome%202015-11-20%2020.21.43.png)

Once on this page, check `Developer mode` to enable loading unpacked extensions. This will allow you to load your extension from a folder. Finally, click `Load unpacked extension`

Run node.js server from this repo https://bitbucket.org/wzup/code-test and you're ready to go.