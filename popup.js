var prev = '';

function setPrev (val) {
    localStorage.removeItem("prev");
    window.localStorage.setItem("prev", val);
}

// window.onbeforeunload = function() {
//     console.log('== onbeforeunload ==: ');
//     return window.location.href
//     setPrev(window.location.href);
// };

// window.onblur = function() {
//     console.log('== onblur ==: ');
//     setPrev(window.location.href);
// }


window.onload = function() {
    console.log("window loaded: ", window.location);
}

chrome.runtime.sendMessage({
    method: 'POST',
    action: 'xhttp',
    url: 'http://localhost:9000/api/links',
    data: JSON.stringify({
        url: window.location.href,
        referrer: document.referrer,
        // previous: window.localStorage.prev
        previous: ''
    })
}, function(responseText) {
    console.log('== responseText ==: ', responseText);
});
